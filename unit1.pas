unit Unit1;

{$mode objfpc}{$H+}
{$codepage utf8}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ComCtrls, StdCtrls,
  utils;

type

  { Tmain }

  Tmain = class(TForm)
    addBt: TButton;
    engInput: TEdit;
    chiInput: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    engWordsMemo: TMemo;
    chiWordsMemo: TMemo;
    nextBt: TButton;
    engWordsLabel: TLabel;
    chiWordsLabel: TLabel;
    pc: TPageControl;
    addPage: TTabSheet;
    learnPage: TTabSheet;
    procedure addBtClick(Sender: TObject);
    procedure chiInputKeyPress(Sender: TObject; var Key: char);
    procedure chiWordsMemoKeyPress(Sender: TObject; var Key: char);
    procedure engWordsMemoKeyPress(Sender: TObject; var Key: char);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure nextBtClick(Sender: TObject);
    procedure pcChange(Sender: TObject);
  private

  public

  end;

var
  main: Tmain;

implementation

{$R *.lfm}

{ Tmain }

procedure Tmain.FormResize(Sender: TObject);
begin
  main.height:=pc.Height;
  main.width:=pc.Width;
end;

procedure Tmain.nextBtClick(Sender: TObject);
begin
  showOrHide;
end;

procedure Tmain.pcChange(Sender: TObject);
begin
  if pc.ActivePage=addPage then
    engInput.SetFocus
  else if pc.ActivePage=learnPage then
    nextBt.SetFocus;
end;

procedure Tmain.FormActivate(Sender: TObject);
begin
  nWords:=0;
  randomize;
  pc.ActivePage:=learnPage;

  if FileExists(dataFile) then
    readDataFile
  else
    save;

  changeWord;

  showOrHide;

  nextBt.SetFocus;


  main.Width := pc.Width;
  main.Height := pc.Height;
end;

procedure Tmain.addBtClick(Sender: TObject);
begin
  if engInput.Text='' then
    showMessage('Please enter English word.')
  else if chiInput.Text='' then
    showMessage('Please enter Chinese word.')
  else
  begin
    nWords:=nWords+1;
    words[nWords].eng:=main.engInput.Text;
    words[nWords].chi:=main.chiInput.Text;
    engInput.Text:='';
    chiInput.Text:='';
    save;

    engInput.SetFocus;
  end; //of if
end;

procedure Tmain.chiInputKeyPress(Sender: TObject; var Key: char);
begin
  if ord(key)=13 then //Enter key is pressed
    addBtClick(nil); //click the "Add" button
end;

procedure Tmain.chiWordsMemoKeyPress(Sender: TObject; var Key: char);
begin
  if ord(key)=13 then
    showOrHide;
end;

procedure Tmain.engWordsMemoKeyPress(Sender: TObject; var Key: char);
begin
  if ord(key)=13 then
    showOrHide;
end;

end.

