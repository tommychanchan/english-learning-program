unit utils;

{$mode objfpc}{$H+}
{$codepage utf8}

interface

uses
  Classes, SysUtils, Dialogs;

const
  maxWords=100000;
  dataFile='eng_words.txt';

type
  wordsType=record
    eng, chi:string;
  end;

  wordsArray=array [1..maxWords] of wordsType;

var
  words:wordsArray;
  nWords, nowIndex:integer;

procedure readDataFile;
procedure showOrHide;
procedure changeWord;
procedure save;

implementation
uses
  unit1;

procedure readDataFile;
var
  f:text;
  i:integer;
begin
  assign(f, dataFile);
  reset(f);
  i:=0;
  while not eof(f) do
  begin
    i:=i+1;
    readln(f, words[i].eng);
    readln(f, words[i].chi);
  end; //of while
  nWords:=i;
  close(f);
end; //of procedure

procedure showOrHide;
begin
  if main.chiWordsMemo.Visible then
  begin
    //change word
    changeWord;
  end; //of if
  main.chiWordsMemo.Visible:=not main.chiWordsMemo.Visible;
  main.nextBt.SetFocus;
end; //of procedure

procedure changeWord;
begin
  nowIndex:=random(nWords)+1;
  main.engWordsMemo.Text:=words[nowIndex].eng;
  main.chiWordsMemo.Text:=words[nowIndex].chi;
end; //of procedure

procedure save;
var
  f:text;
  i:integer;
begin
  assign(f, dataFile);
  rewrite(f);
  for i:=1 to nWords do
  begin
    writeln(f, words[i].eng);
    writeln(f, words[i].chi);
  end; //of for
  close(f);
end; //of procedure



end.

